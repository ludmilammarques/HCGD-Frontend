import { Component, OnInit } from '@angular/core';
import { ThematicListService } from './thematic-list.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-thematic-list',
  templateUrl: './thematic-list.component.html',
  styleUrls: ['./thematic-list.component.scss'],
  providers: [ThematicListService]
})

export class ThematicListComponent implements OnInit {
  formGroup: FormGroup;
  thematics: any[];
  pages: number;
  currentPage = 0;

  constructor(private formBuilder: FormBuilder, private thematicListService: ThematicListService, private router: Router) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      thematicName: ['', Validators.required],
      thematicDescription: [''],
    });

    this.thematicListService.thematics.subscribe(data => {
      this.thematics = data.results.filter(value => value.name.length !== 0);
      this.pages = data.pages;
    });

    this.thematicListService.getThematics();
  }

  saveThematic() {
    if (this.formGroup.valid) {
      var thematicName = this.formGroup.get('thematicName').value.trim();
      var thematicDescription = this.formGroup.get('thematicDescription').value.trim();

      if (!thematicName) {
        return;
      }

      const thematic = {
        name: thematicName,
        description: thematicDescription ? thematicDescription : '-'
      }

      this.thematicListService.saveThematic(thematic).subscribe(_ => {
        this.thematicListService.getThematics(),
        this.formGroup.reset();
      });
    }
  }

  onChangePageClick(event: { page: number; }) {
    if (event.page == this.currentPage || event.page == 0) {
      return;
    }

    this.thematicListService.getThematicNewPage(event.page);
    this.currentPage = event.page;
  }

  onThematicClick(thematic: any) {
    this.router.navigate(['thematicdetail'], {queryParams: {
      thematic_id: thematic.id,
      thematic_name: thematic.name,
      thematic_description: thematic.description
    }});
  }
}

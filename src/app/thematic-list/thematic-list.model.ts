import { Paginated } from '../shared/models/paginated.model';

export interface ThematicResponse extends Paginated<Thematic> {
}

export interface Thematic {
    id: number,
    name: string,
    description: string
}
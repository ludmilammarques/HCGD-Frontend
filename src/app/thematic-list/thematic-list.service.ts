import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize, map } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { ThematicResponse } from './thematic-list.model';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class ThematicListService {
    private thematicsSubject: Subject<ThematicResponse>;
    thematics: Observable<ThematicResponse>;
    nextPage: string;
    previousPage: string;

    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) {
        this.thematicsSubject = new Subject();
        this.thematics = this.thematicsSubject.asObservable();
    }

    getThematics(url: string = endpoint.thematic) {
        this.spinnerService.blockUI();
        return this.httpClient.get<ThematicResponse>(url)
          .pipe(
            map((data) => {
              this.nextPage = data.next;
              this.previousPage = data.previous;
              return data;
            }),
            finalize(() => this.spinnerService.unblockUI()))
          .subscribe((data) => this.thematicsSubject.next(data));
    }

    getThematicNewPage(page: any) {
      var newPage = this.nextPage != null ? this.nextPage : this.previousPage;
      newPage = newPage.replace(/page=[0-9]*/g, "page="+page);
      return this.getThematics(newPage);
    }

    saveThematic(body: any) {
      this.spinnerService.blockUI();
      return this.httpClient.post(endpoint.thematic, body)
          .pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    public getThematicList() {
      return this.httpClient.get<ThematicResponse>(endpoint.thematic);
  }
}

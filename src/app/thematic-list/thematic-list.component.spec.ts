import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThematicListComponent } from './thematic-list.component';

describe('ThematicListComponent', () => {
  let component: ThematicListComponent;
  let fixture: ComponentFixture<ThematicListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThematicListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThematicListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

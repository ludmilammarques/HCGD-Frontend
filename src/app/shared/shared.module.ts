import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatRadioModule } from '@angular/material/radio';
import { FormsModule } from '@angular/forms';

import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ModalComponent } from './components/modal/modal.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import { MediaDialogComponent } from './components/media-dialog/media-dialog.component';

import { SpinnerService } from './components/spinner/spinner.service';
import { ModalService } from './components/modal/modal.service';
import { ErrorHandlerService } from './utils/error-handler.service';
import { AuthGuard } from './guard/auth.guard';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { NumberToArrayPipe } from './utils/number-to-array.pipe';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        MatRadioModule,
        FormsModule
    ],
    exports: [
        SidebarComponent,
        NavbarComponent,
        ModalComponent,
        SpinnerComponent,
        PaginatorComponent,
        NumberToArrayPipe
    ],
    declarations: [
        SidebarComponent,
        NavbarComponent,
        ModalComponent,
        SpinnerComponent,
        MediaDialogComponent,
        PaginatorComponent,
        NumberToArrayPipe
    ],
    entryComponents: [
        MediaDialogComponent,
    ],
    providers: [
        SpinnerService,
        ModalService,
        ErrorHandlerService,
        AuthGuard
    ],
})
export class SharedModule { }

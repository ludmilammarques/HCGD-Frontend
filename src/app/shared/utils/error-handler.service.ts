import { Injectable, ErrorHandler } from '@angular/core';
import { Router } from '@angular/router';

import { ModalService } from '../components/modal/modal.service';

@Injectable()
export class ErrorHandlerService implements ErrorHandler {

    constructor(private modalService: ModalService, private router: Router) {}

    handleError(error: any): any {

        if (error.status !== undefined) {
            const bodyError = JSON.parse(error.text());
            const messagesModal = [];

            switch (error.status) {
                case 0:
                    messagesModal.push('Atenção');
                    messagesModal.push('Verifique sua conexão!');
                    messagesModal.push('');
                    messagesModal.push('Ok');
                    this.modalService.showModal(messagesModal, () => {});
                    break;
                case 401:
                    this.router.navigate(['login']);
                    break;
                case 400:
                case 504:
                case 404:
                case 500:
                    messagesModal.push('Atenção');
                    messagesModal.push('Ocorreu um erro no servidor. Por favor, tente novamente.');
                    messagesModal.push('');
                    messagesModal.push('Ok');
                    this.modalService.showModal(messagesModal, () => {});
                    break;
                default:
                    console.log(error);
            }
        } else {
            console.log(error);
        }
    }
}

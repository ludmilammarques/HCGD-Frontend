export const enum ResourceType {
    Text  = '1',
    Image = '2',
    Audio = '3',
    Video = '4',
}


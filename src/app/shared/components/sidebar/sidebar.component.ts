import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})

export class SidebarComponent implements OnInit {
  name: string;
  description: string;
  avatar: string;
  isHelper: Boolean;

  constructor(private router: Router) { }

  ngOnInit() {
    const user = JSON.parse(localStorage.getItem('user'));
    this.name = user.first_name + ' ' + user.last_name;
    this.isHelper = user.helper_datas && user.helper_datas.length > 0;
    if (!this.isHelper) {
      if (user.supervisor_datas[0]) {
        this.description = user.supervisor_datas[0].occupation.occupation;
      }
    } else {
      this.description = user.helper_datas[0].skill.skill;
    }

    if (user.supervisor_datas && user.supervisor_datas.length > 0 && user.supervisor_datas.avatar_url) {
      this.avatar = user.supervisor_datas[0].avatar_url;
    } else {
      this.avatar = './assets/icons/round/Woman-9.svg';
    }
  }

  imageError(event: any) {
    this.avatar = './assets/icons/round/Woman-9.svg';
  }

  logout() {
    var response = confirm("Deseja realmente sair?");
    if (response) {
      localStorage.clear();
      this.router.navigate(['login']);
    }
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';

import { endpoint } from '../../utils/endpoint';
import { ErrorHandlerService } from '../../utils/error-handler.service';
import { ResourceResponse } from '../../models/resource.model';
import { SpinnerService } from '../spinner/spinner.service';

@Injectable()
export class MediaDialogService {
    constructor(private httpClient: HttpClient, private spinnerService: SpinnerService) { }

    getResources(resourceType: number) {
        const url = endpoint.resourcesByType + resourceType.toString();
        return this.httpClient.get<ResourceResponse>(url).pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    createResource(body: any) {
        return this.httpClient.post<ResourceResponse>(endpoint.resource, body).pipe(finalize(() => this.spinnerService.unblockUI()));
    }
}

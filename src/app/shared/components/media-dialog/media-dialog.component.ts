import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MediaDialogService } from './media-dialog.service';
import { ResourceResponse } from '../../models/resource.model';

@Component({
  selector: 'app-media-dialog',
  templateUrl: './media-dialog.component.html',
  styleUrls: ['./media-dialog.component.scss'],
  providers: [ MediaDialogService ]
})
export class MediaDialogComponent implements OnInit {
  resources: any = [];
  resourceSelected: number;
  resourceType: string;
  newText = '';
  newImage = '';
  newAudio = '';
  newVideo = '';
  institution : any;

  constructor(private dialogRef: MatDialogRef<MediaDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private mediaDialogService: MediaDialogService) { }

  ngOnInit() {
    this.mediaDialogService.getResources(this.data.type)
      .subscribe((data: ResourceResponse) => {
        this.resources = data.results;
        if (this.resources.length > 0) 
        {
          this.resourceType = this.resources[0].resourceType.name;
        } else {
          switch(this.data.type) {
            case 1:
              this.resourceType = 'Texto';
              break;
            case 2:
              this.resourceType = 'Imagem';
              break;
            case 3:
              this.resourceType = 'Áudio';
              break;
            case 4:
              this.resourceType = 'Vídeo';
          }
        }
      });

      const user = JSON.parse(localStorage.getItem('user'));
      var isSupervisor = user.supervisor_datas && user.supervisor_datas.length > 0;
      if (isSupervisor) {
        this.institution = user.supervisor_datas[0].institution.id;
      } else {
        this.institution = 1;
      }
  }

  closeDialog() {
    const result = {
      id: this.resourceSelected,
      content: this.resources.filter(r => r.id == this.resourceSelected)[0].content
    };
    this.dialogRef.close(result);
  }

  createText() {
    if (this.newText) {
      const body = {
        content: this.newText,
        rights: 'copyrights',
        status: true,
        author: parseInt(localStorage.getItem('supervisor_id'), 10),
        resourceType: 1,
        institution: this.institution
      };
      this.mediaDialogService.createResource(body).subscribe(data => {
        this.resources.push(data);
        this.newText = '';
      });
    }
  }

  createImage() {
    if (this.newImage) {
      const body = {
        content: this.newImage,
        rights: 'copyrights',
        status: true,
        author: parseInt(localStorage.getItem('supervisor_id'), 10),
        resourceType: 2,
        institution: this.institution
      };
      this.mediaDialogService.createResource(body).subscribe(data => {
        this.resources.push(data);
        this.newImage = '';
      });
    }
  }

  createAudio() {
    if (this.newAudio) {
      const body = {
        content: this.newAudio,
        rights: 'copyrights',
        status: true,
        author: parseInt(localStorage.getItem('supervisor_id'), 10),
        resourceType: 3,
        institution: this.institution
      };
      this.mediaDialogService.createResource(body).subscribe(data => {
        this.resources.push(data);
        this.newAudio = '';
      });
    }
  }

  createVideo() {
    if (this.newVideo) {
      const body = {
        content: this.newVideo,
        rights: 'copyrights',
        status: true,
        author: parseInt(localStorage.getItem('supervisor_id'), 10),
        resourceType: 4,
        institution: this.institution
      };
      this.mediaDialogService.createResource(body).subscribe(data => {
        this.resources.push(data);
        this.newVideo = '';
      });
    }
  }
}

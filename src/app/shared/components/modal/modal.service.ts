import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ModalService {
    emitModalEvent = new EventEmitter<string[]>();
    emitModalConfirmEvent = new EventEmitter<any>();

    constructor() { }

    showModal(labels: string[], callback: any) {
        this.emitModalEvent.emit(labels);
        this.emitModalConfirmEvent.emit(callback);
    }
}
import { Paginated } from './paginated.model';

export interface ResourceResponse extends Paginated<Resource> {}

interface Resource {
    id: number;
    content: string;
    rights: string;
    status: boolean;
    author: number;
    resourceType: {
        id: number,
        name: string
    };
    institution: number;
}

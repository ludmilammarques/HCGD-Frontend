import { Paginated } from './paginated.model';

export interface SupervisorResponse extends Paginated<Supervisor> {
}

interface Supervisor {
    id: number;
    user: {
        id: number,
        first_name: string,
        last_name: string,
        username: string
    };
    occupation: {
        id: number,
        occupation: string
    };
}
import { Paginated } from './paginated.model';

export interface GroupResponse extends Paginated<Group> {
}

export interface Group {
    id: number,
    name: string,
    description: string
}
import { Paginated } from './paginated.model';

export interface HelperResponse extends Paginated<Helper> {
}

interface Helper {
    id: number;
    user: {
        id: number,
        first_name: string,
        last_name: string,
        username: string
    };
    skill: {
        id: number,
        skill: string
    };
}
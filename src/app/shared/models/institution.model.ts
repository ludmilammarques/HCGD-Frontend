import { Paginated } from './paginated.model';

export interface InstitutionResponse extends Paginated<Institution> {
}

export interface Institution {
    id: number,
    name: string,
}
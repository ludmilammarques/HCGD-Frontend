import { Paginated } from './paginated.model';

export interface PlayerResponse extends Paginated<Player> {
}

export interface Player {
    id: number;
    user: {
        id: number,
        first_name: string,
        last_name: string,
        username: string
    };
    gender: string;
    birthdate: string;
    capability: Capability[];
    group: {
        id: number,
        name: string,
        description: string
    };
}

export interface Capability {
    id: number,
    capability: string
};
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize, map } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { InstitutionResponse } from '../shared/models/institution.model';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { Observable, Subject } from 'rxjs';
import { stringify } from 'querystring';

@Injectable()
export class InstitutionListService {
    private institutionsSubject: Subject<InstitutionResponse>;
    institutions: Observable<InstitutionResponse>;
    nextPage: string;
    previousPage: string;

    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) {
        this.institutionsSubject = new Subject();
        this.institutions = this.institutionsSubject.asObservable();
    }

    getInstitutions(url: string = endpoint.institution) {
        this.spinnerService.blockUI();
        return this.httpClient.get<InstitutionResponse>(url)
          .pipe(
            map((data) => {
              this.nextPage = data.next;
              this.previousPage = data.previous;
              return data;
            }),
            finalize(() => this.spinnerService.unblockUI()))
          .subscribe((data) => this.institutionsSubject.next(data));
    }

    getInstitutionNewPage(page: any) {
      var newPage = this.nextPage != null ? this.nextPage : this.previousPage;
      newPage = newPage.replace(/page=[0-9]*/g, "page="+page);
      return this.getInstitutions(newPage);
    }

    saveInstitution(body: any) {
      this.spinnerService.blockUI();
      return this.httpClient.post(endpoint.institution, body)
          .pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    public getInstitutionList() {
      return this.httpClient.get<InstitutionResponse>(endpoint.institution);
  }
}

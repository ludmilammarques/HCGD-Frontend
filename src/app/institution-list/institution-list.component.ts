import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { InstitutionListService } from './institution-list.service';

@Component({
  selector: 'app-institution-list',
  templateUrl: './institution-list.component.html',
  styleUrls: ['./institution-list.component.scss'],
  providers: [InstitutionListService]
})
export class InstitutionListComponent implements OnInit {
  formGroup: FormGroup;
  institutions: any[];
  pages: number;
  currentPage = 1;

  constructor(private formBuilder: FormBuilder, private institutionListService: InstitutionListService, private router: Router) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      institutionName: ['', Validators.required]
    });

    this.institutionListService.institutions.subscribe(data => {
      this.institutions = data.results.filter(value => value.name.length !== 0);
      this.pages = data.pages;
    });

    this.institutionListService.getInstitutions();
  }

  saveInstitution() {
    if (this.formGroup.valid) {
      const institution = {
        name: this.formGroup.get('institutionName').value.trim()
      }

      this.institutionListService.saveInstitution(institution).subscribe(_ => {
        this.institutionListService.getInstitutions(),
        this.formGroup.reset();
      });
    }
  }

  onChangePageClick(event: { page: number; }) {
    if (event.page == this.currentPage || event.page == 0) {
      return;
    }

    this.institutionListService.getInstitutionNewPage(event.page);
    this.currentPage = event.page;
  }
}

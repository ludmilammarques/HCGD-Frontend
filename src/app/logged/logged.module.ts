import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTabsModule } from '@angular/material/tabs';
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatSelectModule } from '@angular/material/select';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatListModule } from '@angular/material/list';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatMomentDateModule } from '@angular/material-moment-adapter';

import { LoggedRoutingModule } from './logged-routing.module';
import { SharedModule } from '../shared/shared.module';

import { LoggedComponent } from './logged.component';
import { ProjectListComponent } from '../project-list/project-list.component';
import { ProjectDetailComponent } from '../project-detail/project-detail.component';
import { GameDetailComponent } from '../game-detail/game-detail.component';
import { NewProjectComponent } from '../new-project/new-project.component';
import { PlatformDialogComponent } from '../new-project/platform-dialog/platform-dialog.component';
import { PlayerListComponent } from '../player-list/player-list.component';
import { PlayerDetailComponent } from '../player-detail/player-detail.component';
import { NewParameterDialogComponent } from '../new-project/new-parameter-dialog/new-parameter-dialog.component';
import { MediaListComponent } from '../media-list/media-list.component';
import { StepOneComponent } from '../new-project/step-one/step-one.component';
import { StepTwoComponent } from '../new-project/step-two/step-two.component';
import { StepThreeComponent } from '../new-project/step-three/step-three.component';
import { NewPlayerComponent } from '../new-player/new-player.component';
import { NewUserComponent } from '../new-user/new-user.component';
import { UserListComponent } from '../user-list/user-list.component';
import { NewMediaComponent } from '../new-media/new-media.component';
import { GroupListComponent } from '../group-list/group-list.component';
import { GroupDetailComponent } from '../group-detail/group-detail.component';
import { InstitutionListComponent } from '../institution-list/institution-list.component';
import { NewGameComponent } from '../new-game/new-game.component';
import { GameListComponent } from '../game-list/game-list.component';
import { ThematicListComponent } from '../thematic-list/thematic-list.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        FormsModule,
        LoggedRoutingModule,
        MatCardModule,
        MatExpansionModule,
        MatIconModule,
        MatDialogModule,
        MatTabsModule,
        MatStepperModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatSelectModule,
        MatTooltipModule,
        ReactiveFormsModule,
        SharedModule,
        MatGridListModule,
        MatListModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatMomentDateModule
    ],
    exports: [],
    entryComponents: [
        PlatformDialogComponent,
        NewParameterDialogComponent
    ],
    declarations: [
        LoggedComponent,
        ProjectListComponent,
        ProjectDetailComponent,
        GameDetailComponent,
        NewProjectComponent,
        PlatformDialogComponent,
        PlayerListComponent,
        PlayerDetailComponent,
        NewParameterDialogComponent,
        MediaListComponent,
        StepOneComponent,
        StepTwoComponent,
        StepThreeComponent,
        NewPlayerComponent,
        NewUserComponent,
        UserListComponent,
        NewMediaComponent,
        GroupListComponent,
        GroupDetailComponent,
        InstitutionListComponent,
        NewGameComponent,
        GameListComponent,
        ThematicListComponent,
    ],
    providers: [
        MatDatepickerModule,
        MatNativeDateModule
    ],
})
export class LoggedModule { }

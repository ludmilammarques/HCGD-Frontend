import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoggedComponent } from './logged.component';
import { ProjectListComponent } from '../project-list/project-list.component';
import { ProjectDetailComponent } from '../project-detail/project-detail.component';
import { GameDetailComponent } from '../game-detail/game-detail.component';
import { NewProjectComponent } from '../new-project/new-project.component';
import { PlayerListComponent } from '../player-list/player-list.component';
import { PlayerDetailComponent } from '../player-detail/player-detail.component';
import { MediaListComponent } from '../media-list/media-list.component';
import { NewPlayerComponent } from '../new-player/new-player.component';
import { NewUserComponent } from '../new-user/new-user.component';
import { UserListComponent } from '../user-list/user-list.component';
import { NewMediaComponent } from '../new-media/new-media.component';
import { GroupListComponent } from '../group-list/group-list.component';
import { GroupDetailComponent } from '../group-detail/group-detail.component';
import { InstitutionListComponent } from '../institution-list/institution-list.component';
import { NewGameComponent } from '../new-game/new-game.component';
import { GameListComponent } from '../game-list/game-list.component';
import { ThematicListComponent } from '../thematic-list/thematic-list.component';

const routes: Routes = [
  { path: '',
    component: LoggedComponent,
    children: [
      { path: 'projects', component: ProjectListComponent },
      { path: 'projectdetail', component: ProjectDetailComponent },
      { path: 'gamedetail', component: GameDetailComponent },
      { path: 'newproject', component: NewProjectComponent },
      { path: 'players', component: PlayerListComponent },
      { path: 'playerdetail', component: PlayerDetailComponent },
      { path: 'medias', component: MediaListComponent},
      { path: 'newplayer', component: NewPlayerComponent },
      { path: 'newuser', component: NewUserComponent },
      { path: 'users', component: UserListComponent },
      { path: 'newmedia', component: NewMediaComponent },
      { path: 'groups', component: GroupListComponent },
      { path: 'groupdetail', component: GroupDetailComponent },
      { path: 'institutions', component: InstitutionListComponent},
      { path: 'newgame', component: NewGameComponent},
      { path: 'games', component: GameListComponent},
      { path: 'thematic', component: ThematicListComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoggedRoutingModule { }

export const routedComponents = [LoggedComponent];

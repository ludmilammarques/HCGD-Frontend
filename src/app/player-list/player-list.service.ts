import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize, map } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { PlayerResponse } from '../shared/models/player.model';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class PlayerListService {
    private playersSubject: Subject<PlayerResponse>;
    players: Observable<PlayerResponse>;
    nextPage: string;
    previousPage: string;

    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) {
        this.playersSubject = new Subject();
        this.players = this.playersSubject.asObservable();
    }

    getPlayers(url: string = endpoint.players) {
        this.spinnerService.blockUI();
        return this.httpClient.get<PlayerResponse>(url)
          .pipe(
            map((data) => {
              this.nextPage = data.next;
              this.previousPage = data.previous;
              return data;
            }),
            finalize(() => this.spinnerService.unblockUI()))
          .subscribe((data) => this.playersSubject.next(data));
    }

    getPlayerNewPage(page: any) {
      var newPage = this.nextPage != null ? this.nextPage : this.previousPage;
      newPage = newPage.replace(/page=[0-9]*/g, "page="+page);
      return this.getPlayers(newPage);
    }

    public getPlayerList() {
      return this.httpClient.get<PlayerResponse>(endpoint.players);
    }
}


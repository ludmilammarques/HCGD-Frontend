import { Component, OnInit } from '@angular/core';
import { PlayerListService } from './player-list.service';
import { Router } from '@angular/router';
import {GroupListService} from '../group-list/group-list.service';

@Component({
  selector: 'app-player-list',
  templateUrl: './player-list.component.html',
  styleUrls: ['./player-list.component.scss'],
  providers: [PlayerListService, GroupListService]
})
export class PlayerListComponent implements OnInit {
  playersData: any[];
  players: any[];
  groups: any[];
  pages: number;
  groupPages: number;
  currentPage = 0;

  constructor(private playerListService: PlayerListService, private groupListService: GroupListService, private router: Router) { }

  ngOnInit() {
    this.groupListService.getGroupList().subscribe(data => {
      this.groups = data.results.filter(value => value.name !== '');
    });

    this.playerListService.players.subscribe(data => {
      this.players = data.results;
      this.playersData = [];

      this.players.forEach(player => {
        var group: any ;
        if (player.group != null) {
          var groupAux = this.groups.filter(value => value.id == player.group);
          if (groupAux.length > 0) {
            group = groupAux[0];
          } else {
            group = {
              id: 0, 
              name: "", 
              description: ""
            }
          }
        }

        this.playersData.push({
          player_id: player.id,
          player_name: player.user.first_name + " " + player.user.last_name,
          player_group: group,
          player_username: player.user.username,
          player_birthday: player.birthdate
        });
      });

      this.pages = data.pages;
    });

    this.playerListService.getPlayers();
  }

  onChangePageClick(event: { page: number; }) {
    if (event.page == this.currentPage || event.page == 0) {
      return;
    }

    this.playerListService.getPlayerNewPage(event.page);
    this.currentPage = event.page;
  }

  onPlayerClick(player: any) {
    this.router.navigate(['playerdetail'], {queryParams: {
      player_id: player.player_id,
      player_name: player.player_name,
    }});
  }
}

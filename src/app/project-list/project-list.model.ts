import { Paginated } from '../shared/models/paginated.model';

export interface ProjectListResponse extends Paginated<Project> {
}

interface Project {
    id: number;
    name: string;
    objective: string;
    general_task: string;
}

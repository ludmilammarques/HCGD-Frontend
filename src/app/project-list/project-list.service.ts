import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, map, finalize } from 'rxjs/operators';

import { endpoint } from '../shared/utils/endpoint';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { ProjectListResponse } from './project-list.model';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class ProjectListService {
  private supervisorProjectsSubject: Subject<ProjectListResponse>;
  supervisorProjects: Observable<ProjectListResponse>;
  private helperProjectsSubject: Subject<ProjectListResponse>;
  helperProjects: Observable<ProjectListResponse>;

  supervisorNextPage: string;
  supervisorPreviousPage: string;
  helperNextPage: string;
  helperPreviousPage: string;

  constructor(private httpClient: HttpClient, private spinnerService: SpinnerService) {
    this.supervisorProjectsSubject = new Subject();
    this.supervisorProjects = this.supervisorProjectsSubject.asObservable();
    this.helperProjectsSubject = new Subject();
    this.helperProjects = this.helperProjectsSubject.asObservable();
  }

  getSupervisorProjects() {
    this.spinnerService.blockUI();
    const url = endpoint.supervisorProjects + localStorage.getItem('supervisor_id');
    return this.httpClient.get<ProjectListResponse>(url)
      .pipe(
        map((data) => {
          this.supervisorNextPage = data.next;
          this.supervisorPreviousPage = data.previous;
          return data;
        }),
        finalize(() => this.spinnerService.unblockUI())
      ).subscribe((data) => this.supervisorProjectsSubject.next(data));
  }

  getHelperProjects() {
    this.spinnerService.blockUI();
    const url = endpoint.helperProjects + localStorage.getItem('helper_id');
    return this.httpClient.get<ProjectListResponse>(url)
      .pipe(
        map((data) => {
          this.helperNextPage = data.next;
          this.helperPreviousPage = data.previous;
          return data;
        }),
        finalize(() => this.spinnerService.unblockUI())
      ).subscribe((data) => this.helperProjectsSubject.next(data));
  }

  getSupervisorNextPage() {
    this.spinnerService.blockUI();
    const url = this.supervisorNextPage;
    return this.httpClient.get<ProjectListResponse>(url)
      .pipe(
        map((data) => {
          this.supervisorNextPage = data.next;
          this.supervisorPreviousPage = data.previous;
          return data;
        }),
        finalize(() => this.spinnerService.unblockUI()))
      .subscribe((data) => this.supervisorProjectsSubject.next(data));
  }

  getSupervisorPreviousPage() {
    this.spinnerService.blockUI();
    const url = this.supervisorPreviousPage;
    return this.httpClient.get<ProjectListResponse>(url)
      .pipe(
        map((data) => {
          this.supervisorNextPage = data.next;
          this.supervisorPreviousPage = data.previous;
          return data;
        }),
        finalize(() => this.spinnerService.unblockUI()))
      .subscribe((data) => this.supervisorProjectsSubject.next(data));
  }

  getHelperNextPage() {
    this.spinnerService.blockUI();
    const url = this.helperNextPage;
    return this.httpClient.get<ProjectListResponse>(url)
      .pipe(
        map((data) => {
          this.helperNextPage = data.next;
          this.helperPreviousPage = data.previous;
          return data;
        }),
        finalize(() => this.spinnerService.unblockUI()))
      .subscribe((data) => this.helperProjectsSubject.next(data));
  }

  getHelperPreviousPage() {
    this.spinnerService.blockUI();
    const url = this.helperPreviousPage;
    return this.httpClient.get<ProjectListResponse>(url)
      .pipe(
        map((data) => {
          this.helperNextPage = data.next;
          this.helperPreviousPage = data.previous;
          return data;
        }),
        finalize(() => this.spinnerService.unblockUI()))
      .subscribe((data) => this.helperProjectsSubject.next(data));
  }
}

import { Component, OnInit } from '@angular/core';

import { ProjectListService } from './project-list.service';
import { ProjectListResponse } from './project-list.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-project-list',
  templateUrl: './project-list.component.html',
  styleUrls: ['./project-list.component.scss'],
  providers: [ ProjectListService ]
})
export class ProjectListComponent implements OnInit {
  supervisorProjects: any = [];
  helperProjects: any = [];
  supervisor = false;
  helper = false;
  pages: number;
  currentPage = 0;

  constructor(private projectListService: ProjectListService, private router: Router) { }

  ngOnInit() {
    const user = JSON.parse(localStorage.getItem('user'));
    this.supervisor = user.supervisor_datas.length > 0;
    this.helper = user.helper_datas.length > 0;

    if (this.supervisor) {
      this.projectListService.getSupervisorProjects();
      this.projectListService.supervisorProjects
        .subscribe(
          (data: ProjectListResponse) => {
            this.pages = data.pages;
            this.supervisorProjects = data.results;
          }
        );
    }
    if (this.helper) {
      this.projectListService.getHelperProjects();
      this.projectListService.helperProjects
        .subscribe(
          (data: ProjectListResponse) => {
            this.helperProjects = data.results;
          }
        );
    }
  }

  onChangeTab(tab: string) {
    if (tab === 'supervisor') {
      this.supervisor = true;
      this.helper = false;
    } else {
      this.supervisor = false;
      this.helper = true;
    }
  }

  onProjectClick(projectId: number) {
    this.router.navigate(['/projectdetail'], {queryParams: {project_id: projectId}});
  }

  onChangePageClick(event) {
    if (event.page > this.currentPage) {
      if (this.supervisor) {
        this.projectListService.getSupervisorNextPage();
      } else {
        this.projectListService.getHelperNextPage();
      }
    } else {
      if (this.supervisor) {
        this.projectListService.getSupervisorPreviousPage();
      } else {
        this.projectListService.getHelperPreviousPage();
      }
    }
    this.currentPage = event.page;
  }

}

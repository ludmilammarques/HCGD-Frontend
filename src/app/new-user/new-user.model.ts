export interface OccupationResponse {
    count: number;
    next: number;
    previous: number;
    results: Occupation[];
}

interface Occupation {
    id: number;
    occupation: string;
}

export interface SkillResponse {
    count: number;
    next: number;
    previous: number;
    results: Skill[];
}

interface Skill {
    id: number;
    skill: string;
}

export interface Institution {
  id: number,
  name: string,
}
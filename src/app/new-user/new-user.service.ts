import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { OccupationResponse, SkillResponse, Institution } from './new-user.model';
import { finalize } from 'rxjs/operators';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { endpoint } from '../shared/utils/endpoint';
import { Observable } from 'rxjs';

@Injectable()
export class NewUserService {
    constructor(private httpClient: HttpClient, private spinnerService: SpinnerService) { }

    getOccupations() {
        this.spinnerService.blockUI();
        return this.httpClient.get<OccupationResponse>(endpoint.occupations).pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    getSkills() {
        this.spinnerService.blockUI();
        return this.httpClient.get<SkillResponse>(endpoint.skills).pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    saveSupervisor(body: any) {
        this.spinnerService.blockUI();
        return this.httpClient.post(endpoint.supervisors, body)
            .pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    saveHelper(body: any) {
        this.spinnerService.blockUI();
        return this.httpClient.post(endpoint.helpers, body)
            .pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    saveInstitution(body: any) : Observable<Institution> {
      return this.httpClient.post<Institution>(endpoint.institution, body);
    }
}
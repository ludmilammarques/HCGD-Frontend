import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewUserService } from './new-user.service';
import { Router } from '@angular/router';
import { InstitutionListService } from '../institution-list/institution-list.service';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss'],
  providers: [NewUserService, InstitutionListService]
})
export class NewUserComponent implements OnInit {
  formGroup: FormGroup;
  occupations: any[] = [];
  skills: any = [];
  skillSelected: any = [];
  institutions: any[];
  outraInstituicao: boolean;

  constructor(private newUserService: NewUserService, private institutionListService: InstitutionListService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      userFirstName: ['', Validators.required],
      userLastName: ['', Validators.required],
      userEmail: ['', Validators.required],
      username: ['', Validators.required],
      userPassword: ['', Validators.required],
      userRole: ['', Validators.required],
      supervisorInstitution: ['', Validators.required],
      helperInstitution: ['', Validators.required],
      userOccupation: ['', Validators.required],
      userOccupation2: [''],
      avatarUrl: [''],
      moreSkills: [''],
      institutionName: ['']
    });

    this.formGroup.get('supervisorInstitution').valueChanges.subscribe((value) => {
      this.onChangeInstitution(value);
    });

    this.formGroup.get('helperInstitution').valueChanges.subscribe((value) => {
      this.onChangeInstitution(value);
    });

    this.newUserService.getOccupations().subscribe(data => this.occupations = data.results.filter(value => value.occupation !== '')
    .sort((a,b) => a.occupation.localeCompare(b.occupation)));

    this.newUserService.getSkills().subscribe(data => this.skills = data.results.filter(value => value.skill !== '')
      .sort((a,b) => a.skill.localeCompare(b.skill)));

    this.institutionListService.getInstitutionList().subscribe(data => {
      this.institutions = data.results.filter(value => value.name.length !== 0)
        .sort((a,b) => a.name.localeCompare(b.name));
    });
  }

  onChangeInstitution(value: any) {
    if ((Array.isArray(value) && value.includes("Outra")) || value === "Outra") {
      this.outraInstituicao = true;
      this.formGroup.get('institutionName').setValidators(Validators.required);
    } else {
      this.outraInstituicao = false;
      this.formGroup.get('institutionName').clearValidators();
    }
  }

  setSkills(skills: any) {
    this.skillSelected = skills.map(data => {
      const s = { skill: data._element.nativeElement.textContent };
      return s;
    });
  }

  saveNewUser() {
    const user = {
      first_name: this.formGroup.get('userFirstName').value,
      last_name: this.formGroup.get('userLastName').value,
      username: this.formGroup.get('username').value,
      email: this.formGroup.get('userEmail').value,
      password: this.formGroup.get('userPassword').value
    };

    const occupation = this.formGroup.get('userOccupation').value !== 'Outra' 
      ? this.formGroup.get('userOccupation').value 
      : this.formGroup.get('userOccupation2').value

    if (this.formGroup.get('userRole').value === 's') {
      var institution_id : any;
      if (this.outraInstituicao) {
        const institution = {
          name: this.formGroup.get('institutionName').value
        }

        this.newUserService.saveInstitution(institution).subscribe(value => {
          institution_id = value.id;
          
          const supervisor = {
            user: user,
            institution: institution_id,
            avatar_url: this.formGroup.get('avatarUrl').value.trim() !== "" ? this.formGroup.get('avatarUrl').value : "-",
            occupation: {
              occupation: occupation
            }
          };
    
          this.newUserService.saveSupervisor(supervisor).subscribe(_ => this.router.navigate(['users']));
        });
      } else {
        institution_id = this.formGroup.get('supervisorInstitution').value

        const supervisor = {
          user: user,
          institution: institution_id,
          avatar_url: this.formGroup.get('avatarUrl').value.trim() !== "" ? this.formGroup.get('avatarUrl').value : "-",
          occupation: {
            occupation: occupation
          }
        };
  
        this.newUserService.saveSupervisor(supervisor).subscribe(_ => this.router.navigate(['users']));
      }
    } else {
      if (this.outraInstituicao) {
        var institution_list = this.formGroup.get('helperInstitution').value.filter((value: string) => value !== 'Outra');
        const institution = {
          name: this.formGroup.get('institutionName').value
        }

        this.newUserService.saveInstitution(institution).subscribe(value => {
          institution_list.push(value.id.toString());

          this.formGroup.get('moreSkills').value.split(',').forEach((skillStr: string) => {
            var skillStrTrim = skillStr.trim();
            if (skillStrTrim) {
              this.skillSelected.push({skill: skillStrTrim});
            }
          });

          const helper = {
            user: user,
            institution: institution_list,
            skill: this.skillSelected
          };
  
          this.newUserService.saveHelper(helper).subscribe(_ => this.router.navigate(['users']));
        });

      } else {
        this.formGroup.get('moreSkills').value.split(',').forEach((skillStr: string) => {
          var skillStrTrim = skillStr.trim();
          if (skillStrTrim) {
            this.skillSelected.push({skill: skillStrTrim});
          }
        });

        const helper = {
          user: user,
          institution: this.formGroup.get('helperInstitution').value,
          skill: this.skillSelected
        };

        this.newUserService.saveHelper(helper).subscribe(_ => this.router.navigate(['users']));
      }
    }
  }
}

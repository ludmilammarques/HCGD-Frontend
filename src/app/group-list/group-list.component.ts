import { Component, OnInit } from '@angular/core';
import { GroupListService } from './group-list.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-group-list',
  templateUrl: './group-list.component.html',
  styleUrls: ['./group-list.component.scss'],
  providers: [GroupListService]
})

export class GroupListComponent implements OnInit {
  formGroup: FormGroup;
  groups: any[];
  pages: number;
  currentPage = 0;

  constructor(private formBuilder: FormBuilder, private groupListService: GroupListService, private router: Router) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      groupName: ['', Validators.required],
      groupDescription: [''],
    });

    this.groupListService.groups.subscribe(data => {
      this.groups = data.results.filter(value => value.name.length !== 0);
      this.pages = data.pages;
    });

    this.groupListService.getGroups();
  }

  saveGroup() {
    if (this.formGroup.valid) {
      var groupName = this.formGroup.get('groupName').value.trim();
      var groupDescription = this.formGroup.get('groupDescription').value.trim();

      if (!groupName) {
        return;
      }

      if (groupDescription) {
        const group = {
          name: groupName,
          description: groupDescription
        }

        this.groupListService.saveGroup(group).subscribe(_ => {
          this.groupListService.getGroups(),
          this.formGroup.reset();
        });
      } else {
        const group = {
          name: groupName
        }
        this.groupListService.saveGroup(group).subscribe(_ => {
          this.groupListService.getGroups(),
          this.formGroup.reset();
        });
      }
    }
  }

  onChangePageClick(event: { page: number; }) {
    if (event.page == this.currentPage || event.page == 0) {
      return;
    }

    this.groupListService.getGroupNewPage(event.page);
    this.currentPage = event.page;
  }

  onGroupClick(group: any) {
    this.router.navigate(['groupdetail'], {queryParams: {
      group_id: group.id,
      group_name: group.name,
      group_description: group.description
    }});
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize, map } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { GroupResponse } from '../shared/models/group.model';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class GroupListService {
    private groupsSubject: Subject<GroupResponse>;
    groups: Observable<GroupResponse>;
    nextPage: string;
    previousPage: string;

    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) {
        this.groupsSubject = new Subject();
        this.groups = this.groupsSubject.asObservable();
    }

    getGroups(url: string = endpoint.group) {
        this.spinnerService.blockUI();
        return this.httpClient.get<GroupResponse>(url)
          .pipe(
            map((data) => {
              this.nextPage = data.next;
              this.previousPage = data.previous;
              return data;
            }),
            finalize(() => this.spinnerService.unblockUI()))
          .subscribe((data) => this.groupsSubject.next(data));
    }

    getGroupNewPage(page: any) {
      var newPage = this.nextPage != null ? this.nextPage : this.previousPage;
      newPage = newPage.replace(/page=[0-9]*/g, "page="+page);
      return this.getGroups(newPage);
    }

    saveGroup(body: any) {
      this.spinnerService.blockUI();
      return this.httpClient.post(endpoint.group, body)
          .pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    public getGroupList() {
      return this.httpClient.get<GroupResponse>(endpoint.group);
  }
}

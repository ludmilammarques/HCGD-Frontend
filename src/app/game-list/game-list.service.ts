import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize, map } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { GameResponse } from './game-list.model';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class GameListService {
    private gamesSubject: Subject<GameResponse>;
    games: Observable<GameResponse>;
    nextPage: string;
    previousPage: string;

    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) {
        this.gamesSubject = new Subject();
        this.games = this.gamesSubject.asObservable();
    }

    getGames(url: string = endpoint.games) {
        this.spinnerService.blockUI();
        return this.httpClient.get<GameResponse>(url)
          .pipe(
            map((data) => {
              this.nextPage = data.next;
              this.previousPage = data.previous;
              return data;
            }),
            finalize(() => this.spinnerService.unblockUI()))
          .subscribe((data) => this.gamesSubject.next(data));
    }

    getGameNewPage(page: any) {
      var newPage = this.nextPage != null ? this.nextPage : this.previousPage;
      newPage = newPage.replace(/page=[0-9]*/g, "page="+page);
      return this.getGames(newPage);
    }

    public getGameList() {
      return this.httpClient.get<GameResponse>(endpoint.games);
    }
}


import { Paginated } from '../shared/models/paginated.model';

export interface GameResponse extends Paginated<Game> {
}

export interface Game {
    id: number;
    name: string;
    platform: {
        id: number,
        name: string
    };
    mechanic: {
        id: number,
        name: string,
        description: string,
        rules: Rule[],
        components: Component[];
        parameters: Parameter[];
    };
}

export interface Rule {
    id: number;
    rule: string;
}

export interface Component {
    id: number;
    name: string;
    purpose: string;
    tag: string;
    resourceTypes: ResourceType[];
}

export interface Parameter {
    id: number;
    name: string;
    description: string;
}

export interface ResourceType {
    id: number;
    name: string;
}
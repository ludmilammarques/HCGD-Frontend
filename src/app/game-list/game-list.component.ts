import { Component, OnInit } from '@angular/core';
import { GameListService } from './game-list.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss'],
  providers:[GameListService]
})
export class GameListComponent implements OnInit {
  games: any[];
  pages: number;
  currentPage = 0;

  constructor(private gameListService: GameListService, private router: Router) { }

  ngOnInit() {
    this.gameListService.games.subscribe(data => {
      this.games = data.results;
      this.pages = data.pages;
    })

    this.gameListService.getGames();
  }


  onChangePageClick(event: { page: number; }) {
    if (event.page == this.currentPage || event.page == 0) {
      return;
    }

    this.gameListService.getGameNewPage(event.page);
    this.currentPage = event.page;
  }

  
}

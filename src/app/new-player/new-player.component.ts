import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NewPlayerService } from './new-player.service';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { GroupListService } from '../group-list/group-list.service';
import { DateAdapter, MAT_DATE_FORMATS } from "@angular/material/core";
import { AppDateAdapter, APP_DATE_FORMATS} from '../shared/utils/date.adapter';
import { InstitutionListService } from '../institution-list/institution-list.service';

@Component({
  selector: 'app-new-player',
  templateUrl: './new-player.component.html',
  styleUrls: ['./new-player.component.scss'],
  providers: [NewPlayerService, GroupListService, InstitutionListService,
    {
      provide: DateAdapter, useClass: AppDateAdapter
    },
    {
      provide: MAT_DATE_FORMATS, useValue: APP_DATE_FORMATS
    }]
  })

export class NewPlayerComponent implements OnInit {
  formGroup: FormGroup;
  capabilities: any = [];
  capabilitiesSelected: any = [];
  today: any = moment();
  groups: any[];
  institutions: any[];

  constructor(private formBuilder: FormBuilder, private newPlayerService: NewPlayerService, private groupListService: GroupListService, private institutionListService: InstitutionListService, private router: Router) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      playerName: ['', Validators.required],
      playerLastName: ['', Validators.required],
      playerBirthdate: ['', Validators.required],
      playerGender: ['', Validators.required],
      playerEmail: ['', Validators.required],
      playerUsername: ['', Validators.required],
      playerPassword: ['', Validators.required],
      moreCapabilities: ['', Validators.required],
      institutionSelector: ['', Validators.required],
      groupSelector: [''],
      groupName: [''],
      groupDescription: [''],
      institutionName: ['']
    });

    this.formGroup.get('groupSelector').valueChanges.subscribe((value) => {
      this.groupSelector(value);
    });

    this.formGroup.get('institutionSelector').valueChanges.subscribe((value) => {
      this.onChangeInstitution(value);
    });

    this.newPlayerService.getCapabilities().subscribe(data => {
      this.capabilities = data.results.filter(value => value.capability.length !== 0)
        .sort((a,b) => a.capability.localeCompare(b.capability)); 
    });

    this.groupListService.getGroupList().subscribe(data => {
      this.groups = data.results.filter(value => value.name.length !== 0)
        .sort((a,b) => a.name.localeCompare(b.name));
    });

    this.institutionListService.getInstitutionList().subscribe(data => {
      this.institutions = data.results.filter(value => value.name.length !== 0)
        .sort((a,b) => a.name.localeCompare(b.name));
    });
  }

  groupSelector(group: any) {
    if (group === 'Outra') {
      this.formGroup.get('groupName').setValidators(Validators.required);
      this.formGroup.get('groupDescription').setValidators(Validators.required);
    } else {
      this.formGroup.get('groupName').clearValidators();
      this.formGroup.get('groupDescription').clearValidators();
    }
  }

  onChangeInstitution(institution: any) {
    if (institution === 'Outra') {
      this.formGroup.get('institutionName').setValidators(Validators.required);
    } else {
      this.formGroup.get('institutionName').clearValidators();
    }
  }

  setCapabilities(capabilities: any){
    this.capabilitiesSelected = capabilities.map(data => {
      const c = { capability: data._element.nativeElement.textContent };
      return c;
    });
  }

  savePlayer() {
    //institution
    var institution_id : number;
    if (this.formGroup.get('institutionSelector').value === 'Outra') {
      const institution = {
        name: this.formGroup.get('institutionName').value
      }

      this.newPlayerService.saveInstitution(institution).subscribe(value => {
        institution_id = value.id;

        //group
        var group_id : number;
        if (this.formGroup.get('groupSelector').value === 'Outra') {
          const group = {
            name: this.formGroup.get('groupName').value,
            description: this.formGroup.get('groupDescription').value
          }
    
          this.newPlayerService.saveGroup(group).subscribe(value => {
            group_id = value.id;
            this.save(institution_id, group_id);
          });
        } else {
          group_id =  this.formGroup.get('groupSelector').value;
          this.save(institution_id, group_id);
        }
      });
    } else {
      institution_id = this.formGroup.get('institutionSelector').value;

      //group
      var group_id : number;
      if (this.formGroup.get('groupSelector').value === 'Outra') {
        const group = {
          name: this.formGroup.get('groupName').value,
          description: this.formGroup.get('groupDescription').value
        }
  
        this.newPlayerService.saveGroup(group).subscribe(value => {
          group_id = value.id;
          this.save(institution_id, group_id);
        });
      } else {
        group_id =  this.formGroup.get('groupSelector').value;
        this.save(institution_id, group_id);
      }
    }
  }

  save(institution: number, group: number) {
    const user = {
      first_name: this.formGroup.get('playerName').value,
      last_name: this.formGroup.get('playerLastName').value,
      username: this.formGroup.get('playerUsername').value,
      email: this.formGroup.get('playerEmail').value,
      password: this.formGroup.get('playerPassword').value
    }

    this.formGroup.get('moreCapabilities').value.split(',').forEach((capabilityStr: string) => {
      var capabilityStrTrim = capabilityStr.trim();
      if (capabilityStrTrim) {
        this.capabilitiesSelected.push({capability: capabilityStrTrim});
      }
    });

    var formDate = this.formGroup.get('playerBirthdate').value.toLocaleDateString("en-US", {year: "numeric", month: "2-digit", day: "2-digit"})
    const birthdate = formDate.replace(/(\d{2})\/(\d{2})\/(\d{4})/g, '$3-$1-$2');

    const player = {
      user: user,
      institution: institution,
      birthdate: birthdate,
      gender: this.formGroup.get('playerGender').value,
      capability: this.capabilitiesSelected,
      group: group
    };

    this.newPlayerService.savePlayer(player).subscribe(_ => this.router.navigate(['players']));
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { CapabilityResponse, Institution, Group } from './new-player.model';
import { endpoint } from '../shared/utils/endpoint';
import { finalize } from 'rxjs/operators';
import { GroupResponse } from '../shared/models/group.model';
import { Observable } from 'rxjs';

@Injectable()
export class NewPlayerService {
  constructor(private httpClient: HttpClient, private spinnerService: SpinnerService) { }

  getCapabilities() {
      this.spinnerService.blockUI();
      return this.httpClient.get<CapabilityResponse>(endpoint.capabilities).pipe(finalize(() => this.spinnerService.unblockUI()));
      
  }

  savePlayer(body: any) {
      this.spinnerService.blockUI();
      return this.httpClient.post(endpoint.players, body)
          .pipe(finalize(() => this.spinnerService.unblockUI()));
  }

  saveInstitution(body: any) : Observable<Institution> {
    return this.httpClient.post<Institution>(endpoint.institution, body);
  }

  saveGroup(body: any) : Observable<Group> {
    return this.httpClient.post<Group>(endpoint.group, body);
  }

}

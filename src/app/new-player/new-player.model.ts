
export interface CapabilityResponse {
    count: number;
    next: number;
    previous: number;
    results: Capability[];
}

interface Capability {
    id: number;
    capability: string;
}

export interface Institution {
  id: number,
  name: string,
}

export interface Group {
  id: number,
  name: string,
  description: string
}
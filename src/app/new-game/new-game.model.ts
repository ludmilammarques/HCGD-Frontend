export interface Game {
    name: string,
    platform: number,
    mechanic: number,
}

export interface Platform {
    name: string
}

export interface Mechanic {
    components: any[],
    description: string,
    id: number,
    name: string,
    parameters: any[]
    rules: any[]
}

export interface Rule {
    rule : string,
    mechanic: number
}

export interface Component {
    name: string,
    purpose: string,
    tag: string,
    resourceTypes: number[],
    mechanic: number
}

export interface Parameter {
    parameter: {
        name: string,
        description: string,
        type: string,
        mechanic: number,
    },
    attributes: string[],
}
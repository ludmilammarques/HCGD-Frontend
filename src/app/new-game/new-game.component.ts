import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray, Form } from '@angular/forms';
import { Router } from '@angular/router';
import { StepTwoService } from '../new-project/step-two/step-two.service';
import { PlatformResponse } from '../new-project/new-project.model';
import { NewGameService } from './new-game.service';

@Component({
  selector: 'app-new-game',
  templateUrl: './new-game.component.html',
  styleUrls: ['./new-game.component.scss'],
  providers: [NewGameService, StepTwoService]
})

export class NewGameComponent implements OnInit {
  formGroup: FormGroup;
  platforms: any[];
  resourceTypes: any[];
  components : FormArray;
  parameters : FormArray;
  
  constructor(private formBuilder: FormBuilder, private newGameService: NewGameService, private stepTwoService: StepTwoService, private changeDetectorRef: ChangeDetectorRef, private router: Router) { }

  ngOnInit() {
    
    this.stepTwoService.getPlatforms().subscribe((data: PlatformResponse) => this.platforms = data.results);
    
    this.resourceTypes = [
        { id: 1, name: "Texto" },
        { id: 2, name: "Imagem" },
        { id: 3, name: "Audio" },
        { id: 4, name: "Video" },
    ]

    this.formGroup = this.formBuilder.group({
      gameName: ['', Validators.required],
      gamePlatform: ['', Validators.required],
      gamePlatformName: ['', Validators.required],
      mechanicName: ['', Validators.required],
      mechanicDescription: ['', Validators.required],
      mechanicRules: [''],
      components: this.formBuilder.array([]),
      parameters: this.formBuilder.array([])
    });

    this.components = this.formGroup.get('components') as FormArray;
    this.parameters = this.formGroup.get('parameters') as FormArray;
  }

  createComponent() : FormGroup {
    return this.formBuilder.group({
      componentName: ['', Validators.required],
      componentPurpose: ['', Validators.required],
      componentTag: ['', Validators.required],
      componentResourceTypes: ['', Validators.required],
    })
  }

  addComponent() : void {
    this.components.push(this.createComponent());
    this.changeDetectorRef.detectChanges();
  }

  removeComponent(index: number) : void {
    this.components.removeAt(index);
    this.
    
    changeDetectorRef.detectChanges();
  }

  createParameter() : FormGroup {
    return this.formBuilder.group({
      parameterCheckbox: false,
      parameterName: ['', Validators.required],
      parameterDescription: ['', Validators.required],
      parameterType: [''],
      parameterAttributes: [''],
    })
  }

  addParameter() : void {
    this.parameters.push(this.createParameter());
    this.changeDetectorRef.detectChanges();
  }

  removeParameter(index: number) : void{
    this.parameters.removeAt(index);
    this.changeDetectorRef.detectChanges();
  }

  saveNewGame() {
      const mechanic = {
        name: this.formGroup.get("mechanicName").value,
        description: this.formGroup.get("mechanicDescription").value
      }

      this.newGameService.saveMechanic(mechanic).subscribe((mechanic_response) => {
        const mechanic_id = mechanic_response.id;

        this.formGroup.get('mechanicRules').value.split(',').forEach((rule: string) => {
          var ruleTrim = rule.trim();
          if (ruleTrim) {
            this.newGameService.saveRule({
              rule: ruleTrim,
              mechanic: mechanic_id
            });
          }
        });

        this.components.value.forEach((item: any) => {
          var resourceTypes = [];
          item["componentResourceTypes"].forEach((element: string) => {
            resourceTypes.push({
              name: element
            })
          });
    
          const component = {
            name: item["componentName"],
            purpose: item["componentPurpose"],
            tag: item["componentTag"],
            resourceTypes: resourceTypes,
            mechanic: mechanic_id
          }
  
          this.newGameService.saveComponent(component);
        });


        this.parameters.value.forEach((item: any) => {
          var attributes = [];
          item['parameterAttributes'].split(',').forEach((attribute: string) => {
            var attributeTrim = attribute.trim();
            if (attributeTrim) {
              attributes.push(attributeTrim);
            }
          });
    
          const parameter = {
            parameter: {
              name: item["parameterName"],
              description: item["parameterDescription"],
              type: item["parameterType"],
              mechanic: mechanic_id,
            },
            attributes: attributes
          }

          this.newGameService.saveParameter(parameter);
        });

        if (this.formGroup.get("gamePlatform").value !== 'Outra') {
          const game = {
            name: this.formGroup.get("gameName").value,
            platform: this.formGroup.get("gamePlatform").value,
            mechanic: mechanic_id
          }
  
          this.newGameService.saveGame(game).subscribe((_) => {
            this.router.navigate(['games'])
          });

        } else {
          const platform = {
            name: this.formGroup.get("gamePlatformName").value
          }

          this.newGameService.savePlatform(platform).subscribe((platform_response) => {
            const game = {
              name: this.formGroup.get("gameName").value,
              platform: platform_response['id'],
              mechanic: mechanic_id
            }
    
            this.newGameService.saveGame(game).subscribe((_) => {
              this.router.navigate(['games'])
            });
          });
        }
      });
  }
}

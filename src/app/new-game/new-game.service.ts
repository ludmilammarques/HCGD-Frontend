import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { endpoint } from '../shared/utils/endpoint';
import { finalize } from 'rxjs/operators';
import { Mechanic, Rule, Parameter, Game, Platform } from './new-game.model';
import { Observable } from 'rxjs';
import { Component } from '@angular/compiler/src/core';

@Injectable()
export class NewGameService {
    constructor(private httpClient: HttpClient, private spinnerService: SpinnerService) { }

    saveGame(body: any) {
        this.spinnerService.blockUI();
        return this.httpClient.post<Game>(endpoint.newgame, body)
            .pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    savePlatform(body: any) : Observable<Platform> {
        return this.httpClient.post<Platform>(endpoint.platforms, body);
    }

    saveMechanic(body: any) : Observable<Mechanic> {
        return this.httpClient.post<Mechanic>(endpoint.mechanic, body);
    }

    saveRule(body: any) : Observable<Rule> {
        return this.httpClient.post<Rule>(endpoint.rule, body);
    }

    saveComponent(body: any) : Observable<Component> {
        return this.httpClient.post<Component>(endpoint.component, body);
    }

    saveParameter(body: any) : Observable<Parameter> {
        return this.httpClient.post<Parameter>(endpoint.parameter, body);
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { GameSessionResponse } from './player-detail.model';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { Subject, Observable } from 'rxjs';

@Injectable()
export class PlayerDetailService {
    private gameSessionSubject: Subject<GameSessionResponse>;
    gameSessions: Observable<GameSessionResponse>;

    constructor(private httpClient: HttpClient,  private spinnerService: SpinnerService) {
        this.gameSessionSubject = new Subject();
        this.gameSessions = this.gameSessionSubject.asObservable();
    }

    getGameSessions(playerId: number) {
        this.spinnerService.blockUI();
        const url = endpoint.gameSessionByPlayer + playerId;
        this.httpClient.get<GameSessionResponse>(url)
            .pipe(finalize(() => this.spinnerService.unblockUI()))
            .subscribe((data) => this.gameSessionSubject.next(data));
    }
}

import { Paginated } from '../shared/models/paginated.model';
import { Player } from '../shared/models/player.model';

export interface GameSessionResponse extends Paginated<GameSession> {
}

interface GameSession {
    id: number;
    time: string;
    hits: number;
    fails: number;
    score: number;
    feeling_rate: number;
    game: Game;
    player: Player;
}

interface Game {
    name: string;
    platform: Platform;
    mechanic: Mechanic;
}

interface Platform {
    name: string;
}

interface Mechanic {
    name: string;
}
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlayerDetailService } from './player-detail.service';

@Component({
  selector: 'app-player-detail',
  templateUrl: './player-detail.component.html',
  styleUrls: ['./player-detail.component.scss'],
  providers: [PlayerDetailService]
})
export class PlayerDetailComponent implements OnInit {
  gameSessions: any[];
  atcs: any[];
  player: any;
  pages: number;
  playerName: string;

  constructor(private playerDetailService: PlayerDetailService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.playerDetailService.gameSessions.subscribe(data => {
      this.gameSessions = data.results;
      this.pages = data.pages;
      this.player = data.results.length !== 0 ? data.results[0].player : null;
    });

    this.route.queryParams.subscribe((params) => {
      this.playerName = params['player_name'];
      const playerId = params['player_id'];
      this.playerDetailService.getGameSessions(playerId);
    });
  }

}

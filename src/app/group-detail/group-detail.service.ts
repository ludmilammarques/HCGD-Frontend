import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize, map } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { PlayerResponse } from '../shared/models/player.model';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { Observable, Subject } from 'rxjs';

@Injectable()
export class GroupDetailService {
    private playersSubject: Subject<PlayerResponse>;
    players: Observable<PlayerResponse>;
    nextPage: string;
    previousPage: string;

    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) {
        this.playersSubject = new Subject();
        this.players = this.playersSubject.asObservable();
    }
}


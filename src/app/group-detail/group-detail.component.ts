import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PlayerListService } from '../player-list/player-list.service';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.scss'],
  providers: [PlayerListService]
})
export class GroupDetailComponent implements OnInit {
  groupId : number;
  groupName : string;
  groupDescription : string;
  players : any[];

  constructor(private route: ActivatedRoute, private playerService: PlayerListService) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      const groupId = params['group_id'];
      this.groupName = params['group_name'];
      this.groupDescription = params['group_description'];
    });

    this.playerService.getPlayerList().subscribe(data => {
      this.players = data.results.filter(value => value.user.first_name !== ''  && value.group && value.group.id == this.groupId);
    });
  }
}

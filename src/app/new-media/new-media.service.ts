import { HttpClient } from "@angular/common/http";
import { SpinnerService } from "../shared/components/spinner/spinner.service";
import { finalize } from "rxjs/operators";
import { endpoint } from "../shared/utils/endpoint";
import { Injectable } from "@angular/core";

@Injectable()
export class NewMediaService {
    constructor(private httpClient: HttpClient, private spinnerService: SpinnerService) { }

    saveMedia(body: any) {
        this.spinnerService.blockUI();
        return this.httpClient.post(endpoint.resource, body)
            .pipe(finalize(() => this.spinnerService.unblockUI()));
    }
}

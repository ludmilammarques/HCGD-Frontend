import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, MaxLengthValidator } from '@angular/forms';
import { NewMediaService } from './new-media.service';
import { Router } from '@angular/router';
import { InstitutionListService } from '../institution-list/institution-list.service';

@Component({
  selector: 'app-new-media',
  templateUrl: './new-media.component.html',
  styleUrls: ['./new-media.component.scss'],
  providers: [NewMediaService, InstitutionListService]
})
export class NewMediaComponent implements OnInit {
  formGroup: FormGroup;
  selectedType: boolean;
  helper_id: number;
  institutions: any;

  constructor(private formBuilder: FormBuilder, private newMediaService: NewMediaService, private institutionListService: InstitutionListService, private router: Router) 
  { 
    this.selectedType = false;
  }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      resourceType:  ['', Validators.required],
      resourceContent: ['', [Validators.required, Validators.maxLength(256)]],
      resourceInstitution: ['', Validators.required],
      resourceCopyrights: ['', [Validators.required, Validators.maxLength(256)]]
    });

    this.formGroup.get('resourceContent').disable();

    const user = JSON.parse(localStorage.getItem('user'));
    var isHelper = user.helper_datas && user.helper_datas.length > 0;
    if (isHelper) {
      this.helper_id = user.helper_datas[0].id;
      this.institutions = [user.helper_datas[0].institution];
    }
    else {
      this.institutionListService.getInstitutionList().subscribe(response => {
        this.institutions = response.results;
      });
      this.helper_id = 1;
    }
  }

  onResourceTypeChange(resourceType: string) {
    if (resourceType) {
      this.formGroup.get('resourceContent').enable();
    } else {
      this.formGroup.get('resourceContent').disable();
    }
  }

  saveResource() {
    if (this.formGroup.valid) {
      var rights = this.formGroup.get('resourceCopyrights').value.trim();

      const resource = {
        content: this.formGroup.get('resourceContent').value,
        rights: rights ? rights : 'copyrights', 
        status: true, 
        author: this.helper_id, 
        resourceType: this.formGroup.get('resourceType').value,
        institution: this.formGroup.get('resourceInstitution').value
      }

      this.newMediaService.saveMedia(resource).subscribe(_ => this.router.navigate(['medias']));
    }
  }
}

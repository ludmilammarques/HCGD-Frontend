import { Component, OnInit } from '@angular/core';
import { MediaListService } from './media-list.service';
import {ResourceType} from '../shared/utils/resource-type';

@Component({
  selector: 'app-media-list',
  templateUrl: './media-list.component.html',
  styleUrls: ['./media-list.component.scss'],
  providers: [ MediaListService ]
})
export class MediaListComponent implements OnInit {

  images: any[] = [];
  audios: any[] = [];
  videos: any[] = [];
  texts: any[] = [];

  constructor(private mediaListService: MediaListService) { }

  ngOnInit() {
  }

  onResourceTypeChange(resourceType: any) {
    this.mediaListService.getResources(resourceType).subscribe(data => {
      switch (resourceType as string) {
        case ResourceType.Text: {
          this.texts = data.results;
          this.images = [];
          this.audios = [];
          this.videos = [];
          break;
        }
        case ResourceType.Image: {
          this.images = data.results;
          this.audios = [];
          this.texts = [];
          this.videos = [];
          break;
        }
        case ResourceType.Audio: {
          this.texts = [];
          this.images = [];
          this.audios = data.results;
          this.videos = [];
          break;
        }
        case ResourceType.Video: {
          this.texts = [];
          this.images = [];
          this.audios = [];
          this.videos = data.results;
          break;
        }
      }
    });
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { ResourceResponse } from '../shared/models/resource.model';

@Injectable()
export class MediaListService {
    constructor(private httpClient: HttpClient) { }

    getResources(resourceType: number) {
        const url = endpoint.resourcesByType + resourceType.toString();
        return this.httpClient.get<ResourceResponse>(url);
    }
}

import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MediaDialogComponent } from '../../shared/components/media-dialog/media-dialog.component';
import { NewParameterDialogComponent } from '../new-parameter-dialog/new-parameter-dialog.component';

@Component({
  selector: 'app-step-three',
  templateUrl: './step-three.component.html',
  styleUrls: ['./step-three.component.scss']
})
export class StepThreeComponent implements OnInit {
  @Output() saveClick = new EventEmitter<any>();
  @Output() previousStepClick = new EventEmitter<any>();
  @Input() gamesSelecteds: any = [];

  parametersSelecteds: any = [];
  resourcesToComponents: any = [];
  resourcesToParameters: any = [];
  roles: any[] = [{
    name: 'Problema',
    value: 'problem'
  }, {
    name: 'Resposta',
    value: 'answer'
  }, {
    name: 'Opção',
    value: 'option'
  }];

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  openMediaDialog(resourceType: any, component: any, game: any) {
    const dialogRef = this.dialog.open(MediaDialogComponent, {
      data: {type: resourceType.id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const componentResource = {
          component: component.id,
          game: game.id,
          type: resourceType.id,
          resource: result
        };

        const included = this.resourcesToComponents.filter(element => element.game === game.id &&
                                element.component === component.id &&
                                element.resource.id === componentResource.resource.id).length > 0;

        if (!included) {
          this.resourcesToComponents.push(componentResource);
        }
      }
    });
  }

  getComponentResources(gameId: number, componentId: number, type: number): any[] {
    return this.resourcesToComponents.filter(element => element.game === gameId &&
                  element.component === componentId &&
                  element.type === type);
  }

  getComponentContentResources(gameId: number, componentId: number, type: number): string[] {
    return this.resourcesToComponents.filter(element => element.game === gameId &&
                  element.component === componentId &&
                  element.type === type).map(element => element.resource.content);
  }

  getProblemResources(): any[] {
    return this.resourcesToComponents.filter(element => element.role === 'problem');
  }

  removeComponentResource(content: string, componentId: number) {
    const index = this.resourcesToComponents.findIndex(item => item.resource.content === content && item.component === componentId);
    if (index > -1) {
      this.resourcesToComponents.splice(index, 1);
    }
  }

  openNewParameterDialog() {
    const dialogRef = this.dialog.open(NewParameterDialogComponent, {
      data: {mechanic: this.gamesSelecteds[0].mechanic}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
      }
    });
  }

  checkParameter(parameter: any) {
    parameter.isSelected = !parameter.isSelected;
    if (this.parametersSelecteds.includes(parameter)) {
      const index = this.parametersSelecteds.indexOf(parameter);
      if (index > -1) {
        this.parametersSelecteds.splice(index, 1);
      }
    } else {
      this.parametersSelecteds.push(parameter);
    }
  }

  openImageDialog(parameter: any, game: any) {
    const dialogRef = this.dialog.open(MediaDialogComponent, {
      data: {type: 2}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const parameterResource = {
          parameter: parameter.id,
          game: game.id,
          resource: result
        };

        let included = false;
        this.resourcesToParameters.forEach((element, index) => {
          if (element.game === game.id && element.parameter === parameter.id) {
            this.resourcesToParameters[index] = parameterResource;
            included = true;
          }
        });

        if (!included) {
          this.resourcesToParameters.push(parameterResource);
        }
      }
    });
  }

  getParameterContentResource(gameId: number, parameterId: number): String {
    const resource = this.resourcesToParameters.filter(element =>
      element.game === gameId && element.parameter === parameterId);
    if (resource.length > 0) {
      return resource[0].resource.content;
    } else {
      return '';
    }
  }

  emitProjectStepThree() {
    if (this.removeComponentResource) {
      this.saveClick.emit(this.resourcesToComponents);
    } else if (this.resourcesToParameters) {
      this.saveClick.emit(this.resourcesToParameters);
    }
  }

  emitPreviousStep() {
    this.previousStepClick.emit();
  }
}

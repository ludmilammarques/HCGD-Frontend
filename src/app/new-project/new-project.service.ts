import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';

import { endpoint } from '../shared/utils/endpoint';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { ProjectGameResponse } from './new-project.model';

@Injectable()
export class NewProjectService {
    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) { }

    postProjectGame(body: any) {
        this.spinnerService.blockUI();
        return this.httpClient.post<ProjectGameResponse[]>(endpoint.projectGames, body)
            .pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    postComponentResource(body: any) {
        this.spinnerService.blockUI();
        return this.httpClient.post(endpoint.componentResource, body).pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    postProjectGameParameter(body: any[]) {
        this.spinnerService.blockUI();
        return this.httpClient.post(endpoint.gameParameter, body).pipe(finalize(() => this.spinnerService.unblockUI()));
    }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';

import { endpoint } from '../../shared/utils/endpoint';
import { ErrorHandlerService } from '../../shared/utils/error-handler.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { GameResponse, ThematicResponse, PlatformResponse } from '../new-project.model';

@Injectable()
export class StepTwoService {
    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) { }

    public getPlatforms() {
        return this.httpClient.get<PlatformResponse>(endpoint.platforms);
    }

    getGames(platformId: number) {
        this.spinnerService.blockUI();
        return this.httpClient.get<GameResponse>(endpoint.gamesByPlatform + platformId)
            .pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    getThematics() {
        this.spinnerService.blockUI();
        return this.httpClient.get<ThematicResponse>(endpoint.thematic).pipe(finalize(() => this.spinnerService.unblockUI()));
    }
}

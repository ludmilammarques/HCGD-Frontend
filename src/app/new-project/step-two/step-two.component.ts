import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StepTwoService } from './step-two.service';
import { ThematicResponse, GameResponse, PlatformResponse } from '../new-project.model';

@Component({
  selector: 'app-step-two',
  templateUrl: './step-two.component.html',
  styleUrls: ['./step-two.component.scss'],
  providers: [StepTwoService]
})
export class StepTwoComponent implements OnInit {
  @Output() nextStepClick = new EventEmitter<any>();
  @Output() previousStepClick = new EventEmitter<any>();

  platforms: any = [];
  games: any = [];
  thematics: any = [];
  gamesSelecteds: any = [];

  constructor(private stepTwoService: StepTwoService) { }

  ngOnInit() {
    this.stepTwoService.getPlatforms().subscribe((data: PlatformResponse) => this.platforms = data.results);
    this.stepTwoService.getThematics().subscribe((data: ThematicResponse) => this.thematics = data.results);
  }

  checkPlatform(platformId) {
    this.stepTwoService.getGames(platformId).subscribe((data: GameResponse) => {
      this.games = data.results;
      this.gamesSelecteds = [];
    });
  }

  checkGame(game: any) {
    if (this.gamesSelecteds.includes(game)) {
      const index = this.gamesSelecteds.indexOf(game);
      if (index > -1) {
        this.gamesSelecteds.splice(index, 1);
      }
    } else {
      this.gamesSelecteds.push(game);
    }
  }

  checkThematic(thematicId: number, game: any) {
    const index = this.gamesSelecteds.indexOf(game);

    game['thematic'] = thematicId;

    if (index > 0) {
      this.gamesSelecteds[index] = game;
    }
  }

  emitProjectStepTwo() {
    const isValid = this.gamesSelecteds.filter(item => item.thematic !== undefined).length > 0;
    if (isValid) {
      this.nextStepClick.emit(this.gamesSelecteds);
    }
  }

  emitPreviousStep() {
    this.previousStepClick.emit();
  }
}

import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { StepOneService } from './step-one.service';
import { PlayerResponse } from '../../shared/models/player.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SupervisorResponse } from '../../shared/models/supervisor';
import { HelperResponse } from '../../shared/models/helper';

@Component({
  selector: 'app-step-one',
  templateUrl: './step-one.component.html',
  styleUrls: ['./step-one.component.scss'],
  providers: [StepOneService]
})
export class StepOneComponent implements OnInit {
  @Output() nextStepClick = new EventEmitter<any>();

  formGroup: FormGroup;
  players: any = [];
  supervisors: any = [];
  helpers: any = [];
  project: any = {
    players: [],
    supervisors: [],
    helpers: []
  };

  constructor(private formBuilder: FormBuilder, private stepOneService: StepOneService) { }

  ngOnInit() {
    this.formGroup = this.formBuilder.group({
      projectName: ['', Validators.required],
      projectObjective: ['', Validators.required],
      projectGeneralTask: ['', Validators.required]
    });

    const userId = parseInt(localStorage.getItem('supervisor_id'), 10);
    this.stepOneService.getPlayers().subscribe((data: PlayerResponse) => this.players = data.results);
    this.stepOneService.getSupervisors().subscribe((data: SupervisorResponse) => {
      this.supervisors = data.results.filter(r => r.user.id !== userId);
    });

    this.stepOneService.getHelpers().subscribe((data: HelperResponse) => {
      this.helpers = data.results.filter(r => r.user.id !== userId);
    });
  }

  checkPlayer(playerId: number) {
    if (this.project.players.includes(playerId)) {
      const index = this.project.players.indexOf(playerId);
      if (index > -1) {
        this.project.players.splice(index, 1);
      }
    } else {
      this.project.players.push(playerId);
    }
  }

  checkSupervisor(supervisorId: number) {
    if (this.project.supervisors.includes(supervisorId)) {
      const index = this.project.supervisors.indexOf(supervisorId);
      if (index > -1) {
        this.project.supervisors.splice(index, 1);
      }
    } else {
      this.project.supervisors.push(supervisorId);
    }
  }

  checkHelper(helperId: number) {
    if (this.project.helpers.includes(helperId)) {
      const index = this.project.helpers.indexOf(helperId);
      if (index > -1) {
        this.project.helpers.splice(index, 1);
      }
    } else {
      this.project.helpers.push(helperId);
    }
  }

  emitProjectStepOne() {
    this.project['name'] = this.formGroup.get('projectName').value;
    this.project['objective'] = this.formGroup.get('projectObjective').value;
    this.project['generalTask'] = this.formGroup.get('projectGeneralTask').value;
    if (this.formGroup.valid) {
      this.nextStepClick.emit(this.project);
    }
  }
}

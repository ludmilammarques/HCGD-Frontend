import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';

import { endpoint } from '../../shared/utils/endpoint';
import { ErrorHandlerService } from '../../shared/utils/error-handler.service';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';
import { PlayerResponse } from '../../shared/models/player.model';
import { SupervisorResponse } from '../../shared/models/supervisor';
import { HelperResponse } from '../../shared/models/helper';

@Injectable()
export class StepOneService {
    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) { }

    getPlayers() {
        this.spinnerService.blockUI();
        return this.httpClient.get<PlayerResponse>(endpoint.players).pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    getSupervisors() {
        this.spinnerService.blockUI();
        return this.httpClient.get<SupervisorResponse>(endpoint.supervisors).pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    getHelpers() {
        this.spinnerService.blockUI();
        return this.httpClient.get<HelperResponse>(endpoint.helpers).pipe(finalize(() => this.spinnerService.unblockUI()));
    }
}

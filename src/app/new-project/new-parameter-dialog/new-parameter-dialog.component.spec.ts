import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewParameterDialogComponent } from './new-parameter-dialog.component';

describe('NewParameterDialogComponent', () => {
  let component: NewParameterDialogComponent;
  let fixture: ComponentFixture<NewParameterDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewParameterDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewParameterDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

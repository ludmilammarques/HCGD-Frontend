import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { endpoint } from '../../shared/utils/endpoint';
import { SpinnerService } from '../../shared/components/spinner/spinner.service';

@Injectable()
export class NewParameterDialogService {

    constructor(private httpClient: HttpClient, private spinnerService: SpinnerService) { }

    postParameter(body: any) {
        this.spinnerService.blockUI();
        return this.httpClient.post(endpoint.parameter, body).pipe(finalize(() => this.spinnerService.unblockUI()));
    }
}

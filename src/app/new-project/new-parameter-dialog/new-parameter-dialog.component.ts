import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormControl, Validators } from '@angular/forms';
import { NewParameterDialogService } from './new-parameter-dialog.service';

@Component({
  selector: 'app-new-parameter',
  templateUrl: './new-parameter-dialog.component.html',
  styleUrls: ['./new-parameter-dialog.component.scss'],
  providers: [NewParameterDialogService]
})
export class NewParameterDialogComponent implements OnInit {

  attrsCount = 1;
  parameter: any = {};
  attrs: string[] = [];

  constructor(private dialogRef: MatDialogRef<NewParameterDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private newParameterDialogService: NewParameterDialogService) {
    this.parameter.mechanic = data.mechanic.id;
  }

  ngOnInit() {
  }

  addAttr() {
    this.attrsCount++;
  }

  removeAttr() {
    if (this.attrsCount > 0) {
      this.attrsCount--;
      this.attrs.pop();
    }
  }

  onCreateClick() {
    const body = {
      parameter: this.parameter,
      attributes: this.parameter.type === 'symbolic' ? this.attrs : []
    };
    this.newParameterDialogService.postParameter(body).subscribe(() => this.dialogRef.close(true));
  }
}

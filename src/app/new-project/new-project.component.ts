import { Component, OnInit } from '@angular/core';
import { NewProjectService } from './new-project.service';
import { MatStepper } from '@angular/material/stepper';
import { flatMap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-project',
  templateUrl: './new-project.component.html',
  styleUrls: ['./new-project.component.scss'],
  providers: [ NewProjectService ]
})
export class NewProjectComponent implements OnInit {

  project: any = {};
  gamesSelecteds: any = [];
  resourcesToComponents: any = [];
  resourcesToParameters: any = [];
  userId: number;

  constructor(private newProjectService: NewProjectService, private router: Router) {}

  ngOnInit() {
    this.userId = parseInt(localStorage.getItem('supervisor_id'), 10);
  }

  onStepOneNextClick(stepOne, stepper: MatStepper) {
    this.project = stepOne;
    stepper.next();
  }

  onStepTwoNextClick(stepTwo, stepper: MatStepper) {
    this.gamesSelecteds = stepTwo;
    stepper.next();
  }

  onStepThreeSaveClick(stepThree) {
    if (stepThree[0].component) {
      this.resourcesToComponents = stepThree;
    } else {
      this.resourcesToParameters = stepThree;
    }
    this.save();
  }

  onPreviousStepClick(stepper: MatStepper) {
    stepper.previous();
  }

  save() {
    const games = [];
    const thematics = [];

    this.gamesSelecteds.forEach(element => {
      games.push(element.id);
      thematics.push(element.thematic);
    });

    this.project.supervisors.push(this.userId);

    const body = {
      games: games,
      thematics: thematics,
      project: this.project
    };

    this.newProjectService.postProjectGame(body).pipe(
      flatMap((response) => {
        if (this.resourcesToComponents.length > 0) {
          return  this.saveComponentResources(response);
        } else {
          return this.saveParameters(response);
        }
      })
    ).subscribe(_ => this.router.navigate(['projects']));
  }

  saveComponentResources(projectGame: any) {
    const resources: any = [];
    projectGame.forEach(element =>  {
      this.resourcesToComponents.filter(resource => element.game === resource.game)
        .map(resource => {
          resources.push({
            component: resource.component,
            projectGame: element.id,
            resource: parseInt(resource.resource.id, 10),
            related: resource.related,
            role: resource.role,
            score: 0
          });
        });
    });

    return  this.newProjectService.postComponentResource(resources);
  }

  saveParameters(projectGame: any) {
    const parameters: any = [];
    projectGame.forEach(element =>  {
      this.resourcesToParameters.filter(resource => element.game === resource.game)
        .map(resource => {
          parameters.push({
            projectGame: element.id,
            parameter: resource.parameter,
            resource: resource.resource.id,
            score: 0
          });
        });
    });

    return  this.newProjectService.postProjectGameParameter(parameters);
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { endpoint } from '../../shared/utils/endpoint';
import { PlatformResponse } from './platform-dialog.model';

@Injectable()
export class PlatformDialogService {
    constructor(private httpClient: HttpClient) { }

    getPlatforms() {
        return this.httpClient.get<PlatformResponse>(endpoint.platforms);
    }
}

import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { PlatformDialogService } from './platform-dialog.service';

@Component({
  selector: 'app-platform-dialog',
  templateUrl: './platform-dialog.component.html',
  styleUrls: ['./platform-dialog.component.scss'],
  providers: [ PlatformDialogService ]
})
export class PlatformDialogComponent implements OnInit {
  platforms = [];
  platformSelected: number;

  constructor(private dialogRef: MatDialogRef<PlatformDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private platformDialogService: PlatformDialogService) { }

  ngOnInit() {
    this.platformDialogService.getPlatforms()
      .subscribe(data => this.platforms = data.results);
  }

  checkPlatform(platformId) {
    this.platformSelected = platformId;
  }

  closeDialog() {
    const result = this.platforms.filter((element) => element.id === this.platformSelected)[0];
    this.dialogRef.close(result);
  }
}

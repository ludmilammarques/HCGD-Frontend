export interface PlatformResponse {
    count: number;
    next: number;
    previous: number;
    results: Platform[];
}

interface Platform {
    id: string;
    name: string;
}

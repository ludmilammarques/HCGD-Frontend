import { Paginated } from '../shared/models/paginated.model';

export interface GameResponse extends Paginated<Game> {
}

interface Game {
    id: number;
    name: string;
    platform: {
        id: number,
        name: string
    };
    mechanic: {
        id: number,
        description: string,
        rules: Rule[],
        components: Component[];
        parameters: Parameter[];
    };
}

interface Rule {
    id: number;
    rule: string;
}

interface Component {
    id: number;
    name: string;
    score: number;
    purpose: string;
    tag: string;
    resourceTypes: ResourceType[];
}

interface Parameter {
    id: number;
    name: string;
    description: string;
}

interface ResourceType {
    id: number;
    name: string;
}

export interface ThematicResponse extends Paginated<Thematic> {
}

interface Thematic {
    id: number;
    name: string;
    description: string;
}

export interface ProjectGameResponse {
    id: number;
    game: number;
}

export interface PlatformResponse {
    count: number;
    next: number;
    previous: number;
    results: Platform[];
}

export interface Platform {
    id: string;
    name: string;
}

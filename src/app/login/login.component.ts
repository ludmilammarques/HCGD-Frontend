import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { LoginService } from './login.service';
import { LoginResponse } from './login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [ LoginService ]
})
export class LoginComponent implements OnInit {
  credentials: any = {};
  warn: string;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
    localStorage.clear();
  }

  onLogin() {
    if (this.credentials.username && this.credentials.password) {
      this.loginService.login(this.credentials)
        .subscribe(
          (data: LoginResponse) => {
            localStorage.setItem('token', data.token);
            if (data.user.supervisor_datas.length > 0) {
              localStorage.setItem('supervisor_id', data.user.supervisor_datas[0].id.toString());
            }
            if (data.user.helper_datas.length > 0) {
              localStorage.setItem('helper_id', data.user.helper_datas[0].id.toString());
            }

            localStorage.setItem('user', JSON.stringify(data.user));
            if (data.user.supervisor_datas.length > 0 || data.user.helper_datas.length > 0) {
              this.router.navigate(['projects']);
            } else {
              this.warn = 'Usuário sem permissão';
            }
          },
          (error) => {
            if (error.status !== undefined) {
              switch (error.status) {
                case 400:
                  this.warn = 'Usuário ou senha não encontrado';
                  break;
                default:
                  this.warn = 'Não foi possível realizar o login. Verifique sua conexão.';
                  break;
              }
            } else {
              this.warn = 'Não foi possível realizar o login. Verifique sua conexão.';
            }
          }
        );
    } else {
      this.warn = 'Por favor, preencha todos os campos';
    }
  }
}

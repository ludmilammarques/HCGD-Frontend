export interface LoginResponse {
    token: string;
    user: {
        id: number,
        username: string,
        first_name: string,
        last_name: string,
        supervisor_datas: SupervisorResponse[],
        helper_datas: HelperResponse[]
    };
}

interface SupervisorResponse {
    id: number;
    occupation: {
        id: number,
        occupation: string
    };
    institution: {
        name: string
    };
    avatar_url: string;
}

interface HelperResponse {
    id: number;
    skill: {
        id: number,
        skill: string
    };
    institution: {
        name: string
    };
}

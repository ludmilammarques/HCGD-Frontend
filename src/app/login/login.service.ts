import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { finalize } from 'rxjs/operators';

import { endpoint } from '../shared/utils/endpoint';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { LoginResponse } from './login.model';

@Injectable()
export class LoginService {
    constructor(private httpClient: HttpClient,
                private spinnerService: SpinnerService) { }

    login(body: any) {
        this.spinnerService.blockUI();
        return this.httpClient.post<LoginResponse>(endpoint.login, body).pipe(finalize(() => this.spinnerService.unblockUI()));
    }
}

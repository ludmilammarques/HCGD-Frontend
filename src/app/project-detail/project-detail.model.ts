import { Paginated } from '../shared/models/paginated.model';

export interface ProjectDetailResponse extends Paginated<ProjectGame> {
}

interface ProjectGame {
    id: number;
    project: {
        id: number;
        name: string;
        objective: string;
        general_task: string;
        players: Player[];
        helpers: Helper[];
        supervisors: Supervisor[];
    };
    game: {
        id: number,
        name: string,
        platform: {
            id: number,
            name: string
        },
        mechanic: {
            id: number,
            name: string,
            description: string,
            rules: Rule[],
            components: Component[]
        }
    };
    thematic: {
        id: number,
        name: string,
        description: string
    };
}

interface Player {
    id: number;
    user: {
        id: number;
        first_name: string;
        last_name: string;
        username: string;
    };
    gender: string;
    birthdate: string;
    capability: {
        capability: string
    };
    group: {
        id: number
        name: string;
        description: string
    };
}

interface Helper {
    id: number;
    user: {
        id: number;
        first_name: string;
        last_name: string;
        username: string;
    };
    skill: {
        skill: string
    };
}

interface Supervisor {
    id: number;
    user: {
        id: number;
        first_name: string;
        last_name: string;
        username: string;
    };
    occupation: {
        occupation: string
    };
    avatar_url: string;
}

interface Rule {
    id: number;
    rule: string;
}

interface Component {
    id: number;
    score: string;
    purpose: string;
    tag: string;
    resourceType: ResourceType[];
}

interface ResourceType {
    id: number;
    name: string;
}

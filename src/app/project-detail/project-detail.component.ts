import { Component, OnInit } from '@angular/core';

import { ProjectDetailService } from './project-detail.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjectDetailResponse } from './project-detail.model';

@Component({
  selector: 'app-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss'],
  providers: [ ProjectDetailService ]
})
export class ProjectDetailComponent implements OnInit {
  projectGameId: number;
  projectDetail: any = {
    players: [],
    helpers: [],
    supervisors: []
  };
  games: any = [];

  constructor(private projectDetailService: ProjectDetailService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      const projectId = params['project_id'];
      this.projectDetailService.getProjectDetail(projectId)
        .subscribe((data: ProjectDetailResponse) => {
          if (data.results.length > 0) {
            this.projectGameId = data.results[0].id;
            this.projectDetail = data.results[0].project;

            data.results.forEach((item) => {
              item.game['projectGame'] = item.id;
              this.games.push(item.game);
            });
          }
        });
    });
  }

  onGameClick(game: any) {
    this.router.navigate(['gamedetail'], {queryParams: {project_game_id: game.projectGame}});
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';

import { endpoint } from '../shared/utils/endpoint';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { ProjectDetailResponse } from './project-detail.model';

@Injectable()
export class ProjectDetailService {
    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) { }

    getProjectDetail(projectId: number) {
        this.spinnerService.blockUI();
        const url = endpoint.projectsDetail + projectId;
        return this.httpClient.get<ProjectDetailResponse>(url).pipe(
            finalize(() => this.spinnerService.unblockUI())
        );
    }
}

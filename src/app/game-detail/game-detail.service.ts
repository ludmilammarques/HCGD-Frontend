import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, finalize } from 'rxjs/operators';
import { zip } from 'rxjs';

import { endpoint } from '../shared/utils/endpoint';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { GameDetailResponse } from './game-detail.model';

@Injectable()
export class GameDetailService {
    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) { }

    getGameDetail(projectGameId: number) {
        this.spinnerService.blockUI();
        const componentsUrl = endpoint.componentsByGame + projectGameId.toString();
        const parametersUrl = endpoint.parametersByGame + projectGameId.toString();

        /*return zip(
            this.httpClient.get<GameDetailResponse>(componentsUrl),
            this.httpClient.get<GameDetailResponse>(parametersUrl)
        ).pipe(finalize(() => this.spinnerService.unblockUI()));*/

        return this.httpClient.get<GameDetailResponse>(componentsUrl).pipe(finalize(() => this.spinnerService.unblockUI()));
    }

    updateComponentResource(id: number, body: any) {
        this.spinnerService.blockUI();
        const url = endpoint.componentResource + id + '/';
        return this.httpClient.patch(url, body).pipe(
            finalize(() => this.spinnerService.unblockUI())
        );
    }

    deleteParameter(id: number) {
        this.spinnerService.blockUI();
        const url = endpoint.parametersByGame + id + '/';
        return this.httpClient.delete(url).pipe(
            finalize(() => this.spinnerService.unblockUI())
        );
    }
}

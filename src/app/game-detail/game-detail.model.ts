import { Paginated } from '../shared/models/paginated.model';

export interface GameDetailResponse extends Paginated<Game> {
}

interface Game {
    id: number;
    game: GameDetailComponent;
}

interface GameDetailComponent {
    id: number;
    name: string;
    components: Component[];
}

interface GameDetailParameter {
    id: number;
    resources: Resource[];
    parameter: {
        id: number;
        name: string;
        description: string;
        score: number;
    };
}

interface Component {
    id: number;
    purpose: string;
    tag: string;
    resourceType: ResourceType[];
    resources: Resource[];
}

interface Resource {
    id: number;
    content: string;
    rights: string;
    status: boolean;
    resourceType: ResourceType;
    related: {
        id: number;
        content: string;
    };
    role: string;
    score: number;
}

interface ResourceType {
    id: number;
    name: string;
}

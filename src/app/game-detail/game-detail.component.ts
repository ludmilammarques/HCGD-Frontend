import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';

import { GameDetailService } from './game-detail.service';
import { MediaDialogComponent } from '../shared/components/media-dialog/media-dialog.component';

@Component({
  selector: 'app-game-detail',
  templateUrl: './game-detail.component.html',
  styleUrls: ['./game-detail.component.scss'],
  providers: [ GameDetailService ]
})
export class GameDetailComponent implements OnInit {
  gameElements: any = [];
  projectGameId: number;
  gameId: number;
  title = '';
  parametersSelecteds = [];

  constructor(private route: ActivatedRoute, private gameDetailService: GameDetailService, private dialog: MatDialog) { }

  ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      this.projectGameId = params['project_game_id'];
      this.getGameDetail();
    });
  }

  getGameDetail() {
    this.gameDetailService.getGameDetail(this.projectGameId)
      .subscribe((datas) => {
        this.title = 'Componentes';
        this.gameElements = datas.results[0].game.components;
        /*datas.forEach(element => {
          this.gameElements = this.gameElements.concat(element.results);

          if (element.results.length > 0) {
            this.title = element.results[0].parameter !== undefined ? 'Parâmetros' : 'Componentes';
          }
        });*/
      });
  }

  openDialog(componentResource: any) {
    const dialogRef = this.dialog.open(MediaDialogComponent, {
      data: {type: componentResource.resourceType.id}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const body = {
          resource: result.id
        };
        this.gameDetailService.updateComponentResource(componentResource.id, body)
          .subscribe(() => this.getGameDetail());
      }
    });
  }

  openImageDialog(parameter: any, game: any) {
    const dialogRef = this.dialog.open(MediaDialogComponent, {
      data: {type: 2}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        const parameterResource = {
          parameter: parameter.id,
          game: game.id,
          resource: result
        };

        /*let included = false;
        this.resourcesToParameters.forEach((element, index) => {
          if (element.game === game.id && element.parameter === parameter.id) {
            this.resourcesToParameters[index] = parameterResource;
            included = true;
          }
        });

        if (!included) {
          this.resourcesToParameters.push(parameterResource);
        }*/
      }
    });
  }

  checkParameter(parameter) {
    if (this.parametersSelecteds.includes(parameter)) {
      const index = this.parametersSelecteds.indexOf(parameter);
      if (index > -1) {
        this.parametersSelecteds.splice(index, 1);
      }
    } else {
      this.parametersSelecteds.push(parameter);
    }
  }
}

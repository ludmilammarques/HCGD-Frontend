import { Component, OnInit } from '@angular/core';
import { UserListService } from './user-list.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers: [UserListService]
})
export class UserListComponent implements OnInit {
  supervisors: any[];
  helpers: any[];
  supervisorPages: number;
  helperPages: number;
  currentSupervisorPage: number;
  currentHelperPage: number;

  constructor(private userListService: UserListService) { }

  ngOnInit() {
    this.userListService.helpers.subscribe(data => {
      this.helpers = data.results;
      this.helperPages = data.pages;
    });
    this.userListService.getSupervisors();

    this.userListService.supervisors.subscribe(data => {
      this.supervisors = data.results;
      this.supervisorPages = data.pages;
    });
    this.userListService.getHelpers();
  }

  onSupervisorClick(event: { page: number; }) {
    if (event.page == this.currentSupervisorPage || event.page == 0) {
      return;
    }

    this.userListService.getSupervisorNewPage(event.page);
    this.currentSupervisorPage = event.page;
  }

  onHelperClick(event: { page: number; }) {
    if (event.page == this.currentHelperPage || event.page == 0) {
      return;
    }

    this.userListService.getHelperNewPage(event.page);
    this.currentHelperPage = event.page;
  }

  onSupervisorPageClick(event: { page: number; }) {
    if (event.page == this.currentSupervisorPage || event.page == 0) {
      return;
    }

    this.userListService.getSupervisorNewPage(event.page);
    this.currentSupervisorPage = event.page;
  }
  
  onHelperPageClick(event: { page: number; }) {
    if (event.page == this.currentHelperPage || event.page == 0) {
      return;
    }

    this.userListService.getHelperNewPage(event.page);
    this.currentHelperPage = event.page;
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { finalize, map } from 'rxjs/operators';
import { endpoint } from '../shared/utils/endpoint';
import { ErrorHandlerService } from '../shared/utils/error-handler.service';
import { SpinnerService } from '../shared/components/spinner/spinner.service';
import { SupervisorResponse } from '../shared/models/supervisor';
import { HelperResponse } from '../shared/models/helper';
import { Subject, Observable } from 'rxjs';

@Injectable({providedIn: 'root'})
export class UserListService {
    private supervisorsSubject: Subject<SupervisorResponse>;
    private helpersSubject: Subject<HelperResponse>;
    supervisors: Observable<SupervisorResponse>;
    helpers: Observable<HelperResponse>;
    supervisorNextPage: string;
    helperNextPage: string;
    supervisorPreviousPage: string;
    helperPreviousPage: string;

    constructor(private httpClient: HttpClient, private errorHandler: ErrorHandlerService, private spinnerService: SpinnerService) {
        this.supervisorsSubject = new Subject();
        this.supervisors = this.supervisorsSubject.asObservable();
        this.helpersSubject = new Subject();
        this.helpers = this.helpersSubject.asObservable();
    }

    getSupervisors(url: string = endpoint.supervisors) {
        this.spinnerService.blockUI();
        return this.httpClient.get<SupervisorResponse>(url)
            .pipe(
                map((data) => {
                    this.supervisorNextPage = data.next;
                    this.supervisorPreviousPage = data.previous;
                    return data;
                }),
                finalize(() => this.spinnerService.unblockUI()))
            .subscribe((data) => this.supervisorsSubject.next(data));
    }

    getSupervisorNewPage(page: any) {
        var newPage = this.supervisorNextPage != null ? this.supervisorNextPage : this.supervisorPreviousPage;
        newPage = newPage.replace(/page=[0-9]*/g, "page="+page);
        return this.getSupervisors(newPage);
    }

    getHelpers(url: string = endpoint.helpers) {
        this.spinnerService.blockUI();
        return this.httpClient.get<HelperResponse>(url)
            .pipe(
                map((data) => {
                    this.helperNextPage = data.next;
                    this.helperPreviousPage = data.previous;
                    return data;
                }),
                finalize(() => this.spinnerService.unblockUI()))
            .subscribe((data) => this.helpersSubject.next(data));
    }

    getHelperNewPage(page: any) {
        var newPage = this.helperNextPage != null ? this.helperNextPage : this.helperPreviousPage;
        newPage = newPage.replace(/page=[0-9]*/g, "page="+page);
        return this.getHelpers(newPage);
    }
}